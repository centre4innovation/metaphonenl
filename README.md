# MetaphoneNL #
Metaphone/soundex phonetic string normalization for the Dutch language.

Based on Diederik Krols' implementation: https://blogs.u2u.be/diederik/post/Fuzzy-lookup-of-Names-with-a-Dutch-Metaphone-implementation

Note some additional changes: 'z' maps to 's', 'v' maps to 'f'.

## Example ##
```java
class Demo { 
    public static void main(String[] args) {
        MetaphoneNL metaphoneNL = new MetaphoneNL();
        System.out.println("metaphoneNL.normalize(\"vrede\") = " + metaphoneNL.normalize("vrede"));
        System.out.println("metaphoneNL.normalize(\"wrede\") = " + metaphoneNL.normalize("wrede"));
        System.out.println("metaphoneNL.normalize(\"china\") = " + metaphoneNL.normalize("china"));
        System.out.println("metaphoneNL.normalize(\"Ik heet Arvid en ik zou je graag welkom heten bij deze metaphone normalizer!\") \n  = " + metaphoneNL.normalize("Ik heet Arvid en ik zou je graag welkom heten bij deze metaphone normalizer!".toLowerCase()));
    }
}
```

Output
```
metaphoneNL.normalize("vrede") = frd
metaphoneNL.normalize("wrede") = vrd
metaphoneNL.normalize("china") = xn
metaphoneNL.normalize("Ik heet Arvid en ik zou je graag welkom heten bij deze metaphone normalizer!") 
  = ak ht arfd an ak s j grg wlkm htn b ds mtfn nrmlsr 
```

## Credits ##
Arvid Halma (HumanityX, Centre for Innovation, Leiden University)

## License ##
Copyright 2018 Centre for Innovation, Leiden University

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.